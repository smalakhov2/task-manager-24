# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME:** Sergei Malakhov

**E-MAIL:** smalakhov2@rencredit.ru

# SOFTWARE
- Maven
- JDK 1.8
- MS WINDOWS 10/Linux

# PROGRAM BUILD

```bash
mvn clean install
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
