package ru.malakhov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        entities.add(project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (userId.equals(project.getUserId())) entities.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project : entities) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> userProjects = findAll(userId);
        entities.removeAll(userProjects);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project : entities) {
            if (id.equals(project.getId()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Project> userProjects = findAll(userId);
        if (userProjects.size() == 0) return null;
        @Nullable final Project project = userProjects.get(index);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        return project;
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Project project : entities) {
            if (name.equals(project.getName()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        entities.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        entities.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) return null;
        entities.remove(project);
        return project;
    }

}