package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void removeByUserId(@NotNull final String userId) {
        entities.removeIf(session -> userId.equals(session.getUserId()));
    }

    @NotNull
    @Override
    public List<Session> findByUserId(@NotNull final String userId) {
        @NotNull final List<Session> userSessions = new ArrayList<>();
        for (@NotNull Session session : entities) {
            if (userId.equals(session.getUserId())) userSessions.add(session);
        }
        return userSessions;
    }

}