package ru.malakhov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        entities.add(task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        if (userId.equals(task.getUserId())) entities.remove(task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : entities) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Task> userTasks = findAll(userId);
        entities.removeAll(userTasks);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Task task : entities) {
            if (id.equals(task.getId())) {
                if (id.equals(task.getId()) && userId.equals(task.getUserId())) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Task> userTasks = findAll(userId);
        if (userTasks.size() == 0) return null;
        @Nullable final Task task = userTasks.get(index);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        return task;
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Task task : entities) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) return null;
        entities.remove(task);
        return task;
    }

}