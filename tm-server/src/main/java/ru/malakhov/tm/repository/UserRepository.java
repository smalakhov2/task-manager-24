package ru.malakhov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.entity.User;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }

}