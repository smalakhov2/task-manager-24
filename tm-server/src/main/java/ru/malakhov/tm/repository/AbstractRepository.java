package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void add(@NotNull final E entity) {
        entities.add(entity);
    }

    @Override
    public void add(@NotNull final Collection<E> entities) {
        for (@Nullable final E entity : entities) {
            if (entity != null) add(entity);
        }
    }

    @Override
    @SafeVarargs
    public final void add(@NotNull final E... entities) {
        for (@NotNull final E entity : entities) {
             add(entity);
        }
    }

    @Nullable
    @Override
    public E remove(@NotNull E entity) {
        final boolean result = entities.remove(entity);
        if (result) return entity;
        return null;
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @Nullable final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @NotNull
    @Override
    public final List<E> findAll() {
        return entities;
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        for (@NotNull final E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;

    }

    @Override
    public void load(@NotNull final Collection<E> entities) {
        clear();
        add(entities);
    }


    @Override
    @SafeVarargs
    public final void load(@NotNull final E... entities) {
        clear();
        add(entities);
    }

    @Override
    public boolean contains(@NotNull final E entity) {
        return entities.contains(entity);
    }

    @Override
    public boolean contains(@NotNull final String id) {
        return (findById(id) != null);
    }

    @Override
    public int count() {
        return entities.size();
    }

}