package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

}
