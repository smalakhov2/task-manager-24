package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<Session> findByUserId(@NotNull String userId);

}
