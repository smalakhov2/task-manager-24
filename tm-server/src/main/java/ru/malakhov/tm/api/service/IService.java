package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;
import ru.malakhov.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void clear();

    void add(@Nullable E entity);

    void add(@Nullable Collection<E> entities);

    void add(@Nullable E... entities);

    @Nullable
    E removeById(@Nullable String id) throws EmptyIdException;

    @Nullable
    E findById(@Nullable String id) throws EmptyIdException;

    void load(@Nullable Collection<E> entities);

    void load(@Nullable E... entities);

    boolean contains(@Nullable String id);

    boolean contains(@Nullable final E entity);

    int count();

}
