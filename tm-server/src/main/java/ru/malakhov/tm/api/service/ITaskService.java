package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String name) throws AbstractException;

    void create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    void add(@Nullable String userId, @Nullable Task task) throws AbstractException;

    void remove(@Nullable String userId, @Nullable Task task) throws AbstractException;

    @NotNull
    List<Task> findAll(@Nullable String userId) throws EmptyUserIdException;

    void clear(@Nullable String userId) throws EmptyUserIdException;

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    Task findOneByIndex(@Nullable String userId, int index) throws AbstractException;

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name) throws AbstractException;

    @Nullable
    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @Nullable
    Task updateTaskByIndex(
            @Nullable String userId,
            int index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    Task removeOneByIndex(@Nullable String userId, int index) throws AbstractException;

    @Nullable
    Task removeOneByName(@Nullable String userId, @Nullable String name) throws AbstractException;

}
