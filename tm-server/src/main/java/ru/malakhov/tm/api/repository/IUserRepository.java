package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

}
