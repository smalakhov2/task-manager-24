package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISessionEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result clearAllSession(
            @WebParam(name = "session") @Nullable final Session session
    );

    List<Session> getAllSessionList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException;

    @Nullable
    @WebMethod
    Session openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result closeSession(@WebParam(name = "session") @Nullable Session session);

    @NotNull
    @WebMethod
    Result closeSessionAll(@WebParam(name = "session") @Nullable Session session);

    @NotNull
    @WebMethod
    List<Session> getListSession(
            @WebParam(name = "session") @Nullable Session session
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    User getUser(
            @WebParam(name = "session") @Nullable Session session
    ) throws AccessDeniedException, AbstractException;

}
