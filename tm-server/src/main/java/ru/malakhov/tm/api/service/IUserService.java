package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws AbstractException;

    @NotNull
    User findByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User removeByLogin(@Nullable String login) throws EmptyLoginException;

    @NotNull
    User updatePassword(@Nullable String userId, @Nullable String password, @Nullable String newPassword) throws AbstractException;

    @NotNull
    User updateUserInfo(
            @Nullable String userId,
            @Nullable String email,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException;

    @NotNull
    User lockUserByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    User unlockUserByLogin(@Nullable String login) throws AbstractException;

}
