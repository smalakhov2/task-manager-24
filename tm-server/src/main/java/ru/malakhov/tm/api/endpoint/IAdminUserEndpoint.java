package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IAdminUserEndpoint {

    @WebMethod
    @NotNull
    Result clearAllUser(
            @WebParam(name = "session") @Nullable final Session session
    );

    @NotNull
    @WebMethod
    List<User> getAllUserList(
            @WebParam(name = "session") @Nullable Session session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    User lockUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    ) throws AbstractException;

    @NotNull
    @WebMethod
    User unlockUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    ) throws AbstractException;

    @Nullable
    @WebMethod
    User removeUserByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    ) throws AbstractException;

}
