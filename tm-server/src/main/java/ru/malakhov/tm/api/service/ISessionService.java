package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    User checkDataAccess(@Nullable String login, @Nullable String password) throws AbstractException;

    @Nullable
    Session open(@Nullable String login, @Nullable String password) throws AbstractException;

    @Nullable
    Session sing(@Nullable Session session);

    @Nullable
    User getUser(@Nullable Session session) throws AbstractException;

    @NotNull
    String getUserId(@Nullable Session session) throws AccessDeniedException;

    @NotNull
    List<Session> getListSession(@Nullable Session session) throws AccessDeniedException;

    void close(@Nullable Session session) throws AccessDeniedException;

    void closeAll(@Nullable Session session) throws AccessDeniedException;

    void validate(@Nullable Session session) throws AccessDeniedException;

    void validate(@Nullable Session session, @Nullable Role role) throws AbstractException;

    boolean isValid(@Nullable Session session);

    void signOutByLogin(@Nullable String login) throws AbstractException;

    void signOutByUserIdLogin(@Nullable String userId) throws AbstractException;

}
