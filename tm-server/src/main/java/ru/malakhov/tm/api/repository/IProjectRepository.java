package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

}
