package ru.malakhov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.endpoint.AbstractEndpoint;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.repository.ProjectRepository;
import ru.malakhov.tm.repository.SessionRepository;
import ru.malakhov.tm.repository.TaskRepository;
import ru.malakhov.tm.repository.UserRepository;
import ru.malakhov.tm.service.*;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.service.*;

import javax.xml.ws.Endpoint;
import java.lang.reflect.Constructor;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(this, sessionRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAdminService adminService = new AdminService(this);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    private void initEndpoints() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.agafonov.tm.endpoint");
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        for (@NotNull final Class<? extends AbstractEndpoint> clazz : classes) {
            final Constructor<? extends AbstractEndpoint> constructor = clazz.getConstructor(IServiceLocator.class);
            registry(constructor.newInstance(this));
        }
    }

    private void initTestData() {
        try {
            userService.create("admin", "admin", Role.ADMIN);
            userService.create("test", "test");
        } catch (AbstractException e) {
            System.out.println("Error loading test data.");
        }

    }

    private void registry(@Nullable final AbstractEndpoint endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%d/%s?wsdl", host, port, name);
        System.out.println(url);
        Endpoint.publish(url, endpoint);
    }


    public void run() {
        initTestData();
        try {
            propertyService.init();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            initEndpoints();
        } catch (Exception e) {
            throw new RuntimeException("Error! Endpoints, weren't load...", e);
        }
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public IAdminService getAdminService() {
        return adminService;
    }

}