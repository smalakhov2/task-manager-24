package ru.malakhov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof AbstractEntity) {
            @NotNull final AbstractEntity abstractEntity = (AbstractEntity) o;
            return id.equals(abstractEntity.getId());
        }
        return false;
    }

    @NotNull
    public String getId() {
        return id;
    }

    @NotNull
    public void setId(String id) {
        this.id = id;
    }

}