package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumerated.Role;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @Nullable
    private String firstName = "";

    @Nullable
    private String lastName = "";

    @Nullable
    private String middleName = "";

    private Boolean locked = false;

    @NotNull
    private Role role = Role.USER;

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email
    ) {
        this.login = login;
        this.email = email;
        this.passwordHash = passwordHash;
    }

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof User && super.equals(o)) {
            @NotNull final User user = (User) o;
            return Objects.equals(login, user.login)
                    && Objects.equals(passwordHash, user.passwordHash)
                    && Objects.equals(email, user.email);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, passwordHash, email);
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NotNull String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NotNull String email) {
        this.email = email;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NotNull String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NotNull String lastName) {
        this.lastName = lastName;
    }

    @Nullable
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@NotNull String middleName) {
        this.middleName = middleName;
    }

    @NotNull
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

}