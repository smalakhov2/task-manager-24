package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractEntity {

    public static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String userId;

    public Project() {
    }

    public Project(@NotNull final String name) {
        this.name = name;
    }

    public Project(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final String userId
    ) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof Project && super.equals(o)) {
            @NotNull final Project project = (Project) o;
            return Objects.equals(name, project.name)
                    && Objects.equals(description, project.description)
                    && Objects.equals(userId, project.userId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, userId);
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

}