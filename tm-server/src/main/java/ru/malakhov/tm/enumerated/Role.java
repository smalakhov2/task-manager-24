package ru.malakhov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public enum Role implements Serializable {

    ADMIN("Admin"),
    USER("User");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}