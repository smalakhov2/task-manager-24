package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(
            @NotNull final IServiceLocator serviceLocator,
            @NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    @Override
    public User checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        if (user.getLocked()) return null;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        if (passwordHash.equals(user.getPasswordHash())) return user;
        return null;
    }

    @Nullable
    @Override
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sing(session);
    }

    @Nullable
    @Override
    public Session sing(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) throws AbstractException {
        validate(session);
        @NotNull final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final Session session) throws AccessDeniedException {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getListSession(@Nullable final Session session) throws AccessDeniedException {
        validate(session);
        return sessionRepository.findByUserId(session.getUserId());
    }

    @Override
    public void close(@Nullable final Session session) throws AccessDeniedException {
        validate(session);
        sessionRepository.remove(session);
    }

    @Override
    public void closeAll(@Nullable final Session session) throws AccessDeniedException {
        validate(session);
        sessionRepository.clear();
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();
        @Nullable final Session signTempSession = sing(tempSession);
        if (signTempSession == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = signTempSession.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(
            @Nullable final Session session,
            @Nullable final Role role
    ) throws AbstractException {
        if (role == null) throw new AccessDeniedException();
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (AccessDeniedException e) {
            return false;
        }
    }

    @Override
    public void signOutByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserIdLogin(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new UnknownUserException();
        sessionRepository.removeByUserId(userId);
    }

}
