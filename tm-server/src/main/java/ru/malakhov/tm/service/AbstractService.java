package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.entity.AbstractEntity;
import ru.malakhov.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean contains(@Nullable final E entity) {
        if (entity == null) return false;
        return repository.contains(entity);
    }

    @Override
    public boolean contains(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.contains(id);
    }

    @Override
    public int count() {
        return repository.count();
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void add(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        repository.add(entities);
    }

    @Override
    @SafeVarargs
    public final void add(@Nullable final E... entities) {
        if (entities == null) return;
        repository.add(entities);
    }

    @Nullable
    @Override
    public E removeById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public E findById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void load(@Nullable final Collection<E> entities) {
        if (entities == null) return;
        repository.load(entities);
    }

    @Override
    @SafeVarargs
    public final void load(@Nullable final E... entities) {
        if (entities == null) return;
        repository.load(entities);
    }

}