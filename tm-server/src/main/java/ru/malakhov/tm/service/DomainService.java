package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Domain;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.empty.EmptyDomainException;

import java.util.List;

public final class DomainService implements IDomainService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void loadData(@Nullable final Domain domain) throws EmptyDomainException {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        serviceLocator.getSessionService().load(domain.getSessions());
    }

    @Override
    public void saveData(@Nullable final Domain domain) throws EmptyDomainException {
        if (domain == null) throw new EmptyDomainException();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        domain.setProjects(projects);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        domain.setTasks(tasks);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        domain.setUsers(users);
        @NotNull final List<Session> sessions = serviceLocator.getSessionService().findAll();
        domain.setSessions(sessions);
    }

}