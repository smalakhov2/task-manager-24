package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public User findByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new UnknownUserException();
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(
            @Nullable final String login
    ) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User(login,password);
        user.setRole(Role.USER);
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User updatePassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String newPassword
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (!user.getPasswordHash().equals(passwordHash)) throw new AccessDeniedException();
        @Nullable final String newPasswordHash = HashUtil.salt(newPassword);
        if (newPasswordHash == null) throw new AccessDeniedException();
        user.setPasswordHash(newPasswordHash);
        return user;
    }

    @NotNull
    @Override
    public User updateUserInfo(
            @Nullable final String userId,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

}