package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.exception.system.PropertyLoadException;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        try (@Nullable final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME)) {
            if (inputStream == null) throw new PropertyLoadException();
            properties.load(inputStream);
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host", "localhost");
        @Nullable final String envHost = System.getProperty("server.host");
        @NotNull final String value = (envHost == null) ? propertyHost : envHost;
        return value;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("server.port", "80");
        @Nullable final String envPort = System.getProperty("server.port");
        @NotNull final String value = (envPort == null) ? propertyPort : envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        @NotNull final String propertySalt = properties.getProperty("session.salt", "twetr12");
        @Nullable final String envSalt = System.getProperty("session.salt");
        @NotNull final String value = (envSalt == null) ? propertySalt : envSalt;
        return value;
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        @NotNull final String propertyCycle = properties.getProperty("session.cycle", "3435");
        @Nullable final String envCycle = System.getProperty("session.cycle");
        @NotNull final String value = (envCycle == null) ? propertyCycle : envCycle;
        return Integer.parseInt(value);
    }

}