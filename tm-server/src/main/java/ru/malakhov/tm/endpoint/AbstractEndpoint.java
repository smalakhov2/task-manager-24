package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;

public class AbstractEndpoint implements IEndpoint {

    @Nullable
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@Nullable final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


}
