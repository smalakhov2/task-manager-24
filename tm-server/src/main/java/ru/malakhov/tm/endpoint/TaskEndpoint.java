package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.ITaskEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Fail;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.dto.Success;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllTask(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getTaskService().clear();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getAllTaskList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public Result createTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().create(session.getUserId(), name, description);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearTaskList(@WebParam(name = "session") @Nullable final Session session) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getTaskService().clear(session.getUserId());
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskList(
            @WebParam(name = "session") @Nullable final Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task getTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task getTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task getTaskByName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskById(
                session.getUserId(),
                id,
                name,
                description
        );
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskByIndex(
                session.getUserId(),
                index,
                name,
                description
        );
    }

}
