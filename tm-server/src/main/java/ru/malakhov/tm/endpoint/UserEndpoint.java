package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IUserEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.Fail;
import ru.malakhov.tm.dto.Result;
import ru.malakhov.tm.dto.Success;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public User updateUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserInfo(
                session.getUserId(),
                email,
                firstName,
                lastName,
                middleName
        );
    }

    @NotNull
    @Override
    @WebMethod
    public User updateUserPassword(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "newPassword") @Nullable final String newPassword
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updatePassword(
                session.getUserId(),
                password,
                newPassword
        );
    }

    @NotNull
    @Override
    @WebMethod
    public Result registryUser(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) throws AbstractException {
        try {
            serviceLocator.getUserService().create(
                    login,
                    password,
                    email
            );
            return new Success();
        } catch (final AccessDeniedException e) {
            return new Fail(e);
        }
    }

}
