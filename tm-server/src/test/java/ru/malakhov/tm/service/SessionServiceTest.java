package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.bootstrap.Bootstrap;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.SessionRepository;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @NotNull
    private final User user1 = new User("User1", HashUtil.salt("1111"), "1@1.ru");

    @NotNull
    private final User user2 = new User("User2", HashUtil.salt("2222"), "2@2.ru");

    @NotNull
    private final User unknownUser = new User("Uknowm", HashUtil.salt("3333"), "3@3.ru");

    @NotNull
    private final Session sessionOne = new Session(
            System.currentTimeMillis(),
            user1.getId(),
            null
    );

    @NotNull
    private final Session sessionTwo = new Session(
            System.currentTimeMillis(),
            user1.getId(),
            null
    );

    @NotNull
    private final Session sessionThree = new Session(
            System.currentTimeMillis(),
            user2.getId(),
            null
    );

    @NotNull
    private final Session sessionFour = new Session(
            System.currentTimeMillis(),
            user2.getId(),
            null
    );

    @NotNull
    private final Session unknownSession = new Session(
            System.currentTimeMillis(),
            unknownUser.getId(),
            null
    );

    @NotNull
    private final List<Session> user1Sessions = new ArrayList<>(Arrays.asList(sessionOne, sessionTwo));

    @NotNull
    private final List<Session> user2Sessions = new ArrayList<>(Arrays.asList(sessionThree, sessionFour));

    private void loadData() {
        user1.setRole(Role.USER);
        user2.setRole(Role.ADMIN);
        serviceLocator.getUserService().add(user1, user2);
        @Nullable Session singSession = sessionService.sing(sessionOne);
        sessionOne.setSignature(singSession.getSignature());
        singSession = sessionService.sing(sessionTwo);
        sessionTwo.setSignature(singSession.getSignature());
        singSession = sessionService.sing(sessionThree);
        sessionThree.setSignature(singSession.getSignature());
        singSession = sessionService.sing(sessionFour);
        sessionFour.setSignature(singSession.getSignature());
        sessionService.add(user1Sessions);
        sessionService.add(user2Sessions);
    }

    @After
    public void after() {
        sessionService.clear();
    }

    @Test
    public void testCreateEmptyService() {
        Assert.assertEquals(0, sessionService.count());
    }

    @Test
    public void testCheckDataAccess() throws AbstractException {
        loadData();

        @Nullable User user = sessionService.checkDataAccess(user1.getLogin(), "3333");
        Assert.assertNull(user);

        user = sessionService.checkDataAccess(user1.getLogin(), "1111");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.hashCode(), user1.hashCode());
        Assert.assertEquals(user, user1);
    }

    @Test
    public void testCheckDataAccessWithoutLogin() throws AbstractException {
        @Nullable final User user = sessionService.checkDataAccess(null, "1111");
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithoutPassword() throws AbstractException {
        @Nullable final User user = sessionService.checkDataAccess(user1.getId(), null);
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithEmptyLogin() throws AbstractException {
        @Nullable final User user = sessionService.checkDataAccess("", "1111");
        Assert.assertNull(user);
    }

    @Test
    public void testCheckDataAccessWithEmptyPassword() throws AbstractException {
        @Nullable final User user = sessionService.checkDataAccess(user1.getId(), "");
        Assert.assertNull(user);
    }

    @Test(expected = UnknownUserException.class)
    public void testCheckDataAccessWithUnknownLogin() throws AbstractException {
        sessionService.checkDataAccess(unknownUser.getLogin(), "3333");
    }

    @Test
    public void checkOpen() throws AbstractException {
        loadData();
        @Nullable Session session = sessionService.open(user1.getLogin(), "3333");
        Assert.assertNull(session);

        session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals(session.getUserId(), user1.getId());

        @Nullable final Session sessionInStorage = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionInStorage);
        Assert.assertEquals(session.hashCode(), sessionInStorage.hashCode());
        Assert.assertEquals(session, sessionInStorage);
    }

    @Test
    public void testOpenWithoutLogin() throws AbstractException {
        @Nullable final Session session = sessionService.open(null, "1111");
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithoutPassword() throws AbstractException {
        @Nullable final Session session = sessionService.open(user1.getId(), null);
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithEmptyLogin() throws AbstractException {
        @Nullable final Session session = sessionService.open("", "1111");
        Assert.assertNull(session);
    }

    @Test
    public void testOpenWithEmptyPassword() throws AbstractException {
        @Nullable final Session session = sessionService.open(user1.getId(), "");
        Assert.assertNull(session);
    }

    @Test(expected = UnknownUserException.class)
    public void testOpenWithUnknownLogin() throws AbstractException {
        sessionService.open(unknownUser.getLogin(), "3333");
    }

    @Test
    public void testSing() {
        @Nullable Session session = sessionService.sing(null);
        Assert.assertNull(session);

        session = sessionService.sing(sessionOne);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getSignature());
        @Nullable final Session resignSession = sessionService.sing(sessionOne);
        Assert.assertNotNull(resignSession);
        Assert.assertEquals(session.getSignature(), resignSession.getSignature());
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUser() throws AbstractException {
        loadData();

        @Nullable final User user = sessionService.getUser(sessionOne);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.hashCode(), user1.hashCode());
        Assert.assertEquals(user, user1);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithoutSession() throws AbstractException {
        sessionService.getUser(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.getUser(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserId() throws AbstractException {
        loadData();

        @Nullable final String userId = sessionService.getUserId(sessionOne);
        Assert.assertNotNull(userId);
        Assert.assertEquals(userId, user1.getId());
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithoutSession() throws AbstractException {
        sessionService.getUserId(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetUserIdWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.getUserId(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSession() throws AccessDeniedException {
        loadData();

        @NotNull final List<Session> sessions = sessionService.getListSession(sessionOne);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessions.hashCode(), user1Sessions.hashCode());
        Assert.assertEquals(sessions, user1Sessions);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithoutSession() throws AbstractException {
        sessionService.getListSession(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testGetListSessionWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.getListSession(session);
    }

    @Test(expected = AccessDeniedException.class

    )
    public void testClose() throws AbstractException {
        loadData();

        sessionService.close(sessionOne);
        @Nullable final Session session = sessionService.findById(sessionOne.getId());
        Assert.assertNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithoutSession() throws AbstractException {
        sessionService.close(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.close(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAll() throws AbstractException {
        loadData();

        sessionService.closeAll(sessionOne);
        for (@NotNull final Session session : user1Sessions) {
            @Nullable final Session sessionInStorage = sessionService.findById(session.getId());
            Assert.assertNull(sessionInStorage);
        }
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithoutSession() throws AbstractException {
        sessionService.closeAll(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testCloseAllWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.closeAll(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidate() throws AccessDeniedException {
        loadData();
        sessionService.validate(sessionOne);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithoutSession() throws AbstractException {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRole() throws AbstractException {
        loadData();
        sessionService.validate(sessionOne, Role.USER);
        sessionService.validate(sessionThree, Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithoutSession() throws AbstractException {
        sessionService.validate(null, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithoutRole() throws AbstractException {
        sessionService.validate(sessionOne, null);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionWithoutUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId(null);
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionWithoutTimestamp() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setTimestamp(null);
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionWithoutSignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature(null);
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionEmptyUserId() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setUserId("");
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithSessionEmptySignature() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void testValidateWithRoleWithIncorrectRole() throws AbstractException {
        loadData();

        @Nullable final Session session = sessionService.open(user1.getLogin(), "1111");
        Assert.assertNotNull(session);
        session.setSignature("");
        sessionService.validate(session, Role.ADMIN);
    }

    @Test
    public void testSignOutByLogin() throws AbstractException {
        loadData();

        sessionService.signOutByLogin(user1.getLogin());
        for (@NotNull final Session session : user1Sessions) {
            @Nullable final Session sessionInStorage = sessionService.findById(session.getId());
            Assert.assertNull(sessionInStorage);
        }
    }

    @Test(expected = EmptyLoginException.class)
    public void testSignOutByLoginWithoutLogin() throws AbstractException {
        sessionService.signOutByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testSignOutByLoginWithEmptyLogin() throws AbstractException {
        sessionService.signOutByLogin("");
    }

    @Test(expected = UnknownUserException.class)
    public void testSignOutByLoginWithUnknownLogin() throws AbstractException {
        sessionService.signOutByLogin(unknownUser.getLogin());
    }

    @Test
    public void testSignOutByUserId() throws AbstractException {
        loadData();

        sessionService.signOutByUserIdLogin(user1.getId());
        for (@NotNull final Session session : user1Sessions) {
            @Nullable final Session sessionInStorage = sessionService.findById(session.getId());
            Assert.assertNull(sessionInStorage);
        }
    }

    @Test(expected = EmptyUserIdException.class)
    public void testSignOutByUserIdWithoutUserId() throws AbstractException {
        sessionService.signOutByUserIdLogin(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testSignOutByUserIdWithEmptyUserId() throws AbstractException {
        sessionService.signOutByUserIdLogin("");
    }

    @Test(expected = UnknownUserException.class)
    public void testSignOutByUserIdWithUnknownUserId() throws AbstractException {
        sessionService.signOutByUserIdLogin(unknownUser.getId());
    }

}