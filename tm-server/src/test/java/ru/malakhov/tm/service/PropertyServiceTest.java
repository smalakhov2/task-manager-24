package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.exception.system.PropertyLoadException;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Formatter;

@Category(DataCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String NAME_PROPERTY_FILE = "/application.properties";

    @NotNull
    private final String DEFAULT_SERVER_HOST = "localhost";

    @NotNull
    private final Integer DEFAULT_SERVER_PORT = 80;

    @NotNull
    private final String DEFAULT_SESSION_SALT = "twetr12";

    @NotNull
    private final Integer DEFAULT_SESSION_CYCLE = 3435;

    @NotNull
    private final String SERVER_HOST = "127.0.0.1";

    @NotNull
    private final Integer SERVER_PORT = 8080;

    @NotNull
    private final String SESSION_SALT = "astytrur145";

    @NotNull
    private final Integer SESSION_CYCLE = 234;

    @Test
    public void testCheckDefaultValues() {
        Assert.assertEquals(DEFAULT_SERVER_HOST, propertyService.getServerHost());
        Assert.assertEquals(DEFAULT_SERVER_PORT, propertyService.getServerPort());
        Assert.assertEquals(DEFAULT_SESSION_CYCLE, propertyService.getSessionCycle());
        Assert.assertEquals(DEFAULT_SESSION_SALT, propertyService.getSessionSalt());
    }

    @Test(expected = PropertyLoadException.class)
    public void testInitPropertyWithoutPropertyFile() throws Exception {
        @Nullable final URL filePath = PropertyService.class.getResource(NAME_PROPERTY_FILE);
        Assert.assertNotNull(filePath);
        @Nullable final File file = new File(filePath.getFile());
        @Nullable final File newFile = new File(filePath.getFile() + "1");
        file.renameTo(newFile);

        try {
            propertyService.init();
        } catch (PropertyLoadException e) {
            throw e;
        } finally {
            newFile.renameTo(file);
        }
    }

    @Test
    public void testInitPropertyWithPropertyFile() throws Exception {
        @Nullable final URL filePath = PropertyService.class.getResource(NAME_PROPERTY_FILE);
        Assert.assertNotNull(filePath);
        @Nullable final File file = new File(filePath.getFile());
        @Nullable final File newFile = new File(filePath.getFile() + "1");
        file.renameTo(newFile);

        @NotNull final StringBuilder stringBuilder = new StringBuilder();
        @NotNull final Formatter formatter = new Formatter(stringBuilder);
        formatter.format("server.port=%d\n", SERVER_PORT);
        formatter.format("server.host=%s\n", SERVER_HOST);
        formatter.format("session.cycle=%d\n", SESSION_CYCLE);
        formatter.format("session.salt=%s", SESSION_SALT);

        try (
                @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        ) {
            fileOutputStream.write(stringBuilder.toString().getBytes());
        }

        propertyService.init();
        Assert.assertEquals(SERVER_HOST, propertyService.getServerHost());
        Assert.assertEquals(SERVER_PORT, propertyService.getServerPort());
        Assert.assertEquals(SESSION_CYCLE, propertyService.getSessionCycle());
        Assert.assertEquals(SESSION_SALT, propertyService.getSessionSalt());
        file.delete();
        newFile.renameTo(file);
    }

}