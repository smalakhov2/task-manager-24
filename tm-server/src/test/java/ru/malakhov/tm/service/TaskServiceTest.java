package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyTaskException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;
import ru.malakhov.tm.exception.unknown.UnknownTaskException;
import ru.malakhov.tm.repository.TaskRepository;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final User user1 = new User("User1", HashUtil.salt("1111"), "1@1.ru");

    @NotNull
    private final User user2 = new User("User2", HashUtil.salt("2222"), "2@2.ru");

    @NotNull
    private final User unknownUser = new User("UnknownUser", HashUtil.salt("3333"), "3@3.ru");

    @NotNull
    private final Task task1 = new Task("Task1", null, user1.getId());

    @NotNull
    private final Task task2 = new Task("Task2", null, user1.getId());

    @NotNull
    private final Task task3 = new Task("Task3", null, user2.getId());

    @NotNull
    private final Task task4 = new Task("Task4", null, user2.getId());

    @NotNull
    final Task unknownTask = new Task("Unknown", null, unknownUser.getId());

    @NotNull
    private final List<Task> user1Tasks = new ArrayList<>(Arrays.asList(task1, task2));

    @NotNull
    private final List<Task> user2Tasks = new ArrayList<>(Arrays.asList(task3, task4));

    private void loadData() {
        taskService.add(user1Tasks);
        taskService.add(user2Tasks);
    }

    @After
    public void after() {
        taskService.clear();
    }

    @Test
    public void testCreateEmptyService() {
        Assert.assertEquals(0, taskService.count());
    }

    @Test
    public void testCreate() throws AbstractException {
        @NotNull final String userId = task1.getUserId();
        @NotNull final String taskName = task1.getName();

        taskService.create(userId, taskName);
        @NotNull final Task task = taskService.findOneByName(userId, taskName);
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithoutUserId() throws AbstractException {
        taskService.create(null, task1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithoutName() throws AbstractException {
        taskService.create(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithEmptyUserId() throws AbstractException {
        taskService.create("", task1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithEmptyName() throws AbstractException {
        taskService.create(task1.getUserId(), "");
    }

    @Test
    public void testCreateWithDescription() throws AbstractException {
        @NotNull final String userId = task1.getUserId();
        @NotNull final String taskName = task1.getName();
        @Nullable final String taskDescription = task1.getDescription();

        taskService.create(userId, taskName, taskDescription);
        @NotNull final Task task = taskService.findOneByName(userId, taskName);
        Assert.assertEquals(1, taskService.count());
        Assert.assertEquals(userId, task.getUserId());
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionWithoutUserId() throws AbstractException {
        taskService.create(null, task1.getName(), task1.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionWithoutName() throws AbstractException {
        taskService.create(task1.getUserId(), null, task1.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testCreateWithDescriptionEmptyUserId() throws AbstractException {
        taskService.create("", task1.getName(), task1.getDescription());
    }

    @Test(expected = EmptyNameException.class)
    public void testCreateWithDescriptionEmptyName() throws AbstractException {
        taskService.create(task1.getUserId(), "", task1.getDescription());
    }

    @Test
    public void testAdd() throws AbstractException {
        taskService.add(task1.getUserId(), task1);
        Assert.assertEquals(1, taskService.count());
        @Nullable final Task task = taskService.findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testAddWithoutUserId() throws AbstractException {
        taskService.add(null, task1);
    }

    @Test(expected = EmptyTaskException.class)
    public void testAddWithoutTask() throws AbstractException {
        taskService.add(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testAddWithEmptyUserId() throws AbstractException {
        taskService.add("", task1);
    }

    @Test
    public void testRemove() throws AbstractException {
        loadData();
        int counter = taskService.count();

        taskService.remove(unknownTask.getUserId(), unknownTask);
        Assert.assertEquals(counter, taskService.count());

        taskService.remove(task1.getUserId(), task1);
        Assert.assertEquals(counter - 1, taskService.count());
        @Nullable final Task task = taskService.findById(task1.getId());
        Assert.assertNull(task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveWithoutUserId() throws AbstractException {
        taskService.remove(null, task1);
    }

    @Test(expected = EmptyTaskException.class)
    public void testRemoveWithoutTask() throws AbstractException {
        taskService.remove(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveWithEmptyUserId() throws AbstractException {
        taskService.remove("", task1);
    }

    @Test
    public void testFindAll() throws EmptyUserIdException {
        loadData();

        @NotNull List<Task> tasks = taskService.findAll(unknownTask.getUserId());
        Assert.assertEquals(0, tasks.size());

        tasks = taskService.findAll(task1.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(user1Tasks.hashCode(), tasks.hashCode());
        Assert.assertEquals(user1Tasks.size(), tasks.size());
        Assert.assertEquals(user1Tasks, tasks);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllWithoutUserId() throws EmptyUserIdException {
        taskService.findAll(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindAllWithEmptyUserId() throws EmptyUserIdException {
        taskService.findAll("");
    }

    @Test
    public void testClear() throws EmptyUserIdException {
        loadData();
        int counter = taskService.count();

        taskService.clear(unknownTask.getUserId());
        Assert.assertEquals(counter, taskService.count());

        taskService.clear(task1.getUserId());
        counter -= user1Tasks.size();
        Assert.assertEquals(counter, taskService.count());
        for (@NotNull final Task task : user1Tasks) {
            Assert.assertFalse(taskService.contains(task));
        }
    }

    @Test(expected = EmptyUserIdException.class)
    public void testClearWithoutUserId() throws EmptyUserIdException {
        taskService.clear(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testClearWithEmptyUserId() throws EmptyUserIdException {
        taskService.clear("");
    }

    @Test
    public void testFindById() throws AbstractException {
        loadData();
        @NotNull final String userId = task1.getUserId();
        @NotNull final String id = task1.getId();

        @NotNull final Task task = taskService.findOneById(userId, id);
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIdWithoutUserId() throws AbstractException {
        loadData();
        taskService.findOneById(null, task1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdWithoutId() throws AbstractException {
        loadData();
        taskService.findOneById(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIdWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.findOneById("", task1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdWithEmptyId() throws AbstractException {
        loadData();
        taskService.findOneById(task1.getUserId(), "");
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByIdWithUnknownData() throws AbstractException {
        loadData();
        taskService.findOneById(unknownTask.getUserId(), unknownTask.getId());
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByIdWithUnknownUserId() throws AbstractException {
        loadData();
        taskService.findOneById(unknownTask.getUserId(), task1.getId());
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByIdWithUnknownId() throws AbstractException {
        loadData();
        taskService.findOneById(task1.getUserId(), unknownTask.getId());
    }

    @Test
    public void testFindByIndex() throws AbstractException {
        loadData();

        @Nullable final Task task = taskService.findOneByIndex(task1.getUserId(), 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIndexWithoutUserId() throws AbstractException {
        loadData();
        taskService.findOneByIndex(null, 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.findOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexWithIndexLessRange() throws AbstractException {
        taskService.findOneByIndex(task1.getUserId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexWithIndexMoreRange() throws AbstractException {
        loadData();
        taskService.findOneByIndex(task1.getUserId(), 10);
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByIndexWithUnknownUserId() throws AbstractException {
        loadData();
        taskService.findOneByIndex(unknownTask.getUserId(), 0);
    }

    @Test
    public void testFindByName() throws AbstractException {
        loadData();
        @NotNull final String userId = task1.getUserId();
        @NotNull final String name = task1.getName();

        @NotNull final Task task = taskService.findOneByName(userId, name);
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByNameWithoutUserId() throws AbstractException {
        loadData();
        taskService.findOneByName(null, task1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindByNameWithoutName() throws AbstractException {
        loadData();
        taskService.findOneByName(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testFindByNameWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.findOneByName("", task1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testFindByNameWithEmptyName() throws AbstractException {
        loadData();
        taskService.findOneByName(task1.getUserId(), "");
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByNameWithUnknownData() throws AbstractException {
        loadData();
        taskService.findOneByName(unknownTask.getUserId(), unknownTask.getName());
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByNameWithUnknownUserId() throws AbstractException {
        loadData();
        taskService.findOneByName(unknownTask.getUserId(), task1.getName());
    }

    @Test(expected = UnknownTaskException.class)
    public void testFindByIdWithUnknownName() throws AbstractException {
        loadData();
        taskService.findOneByName(task1.getUserId(), unknownTask.getName());
    }

    @Test
    public void testRemoveById() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownId = unknownTask.getId();
        @NotNull final String userId = task1.getUserId();
        @NotNull final String id = task1.getId();
        final int counter = taskService.count();

        @Nullable Task task = taskService.removeOneById(unknownUserId, unknownId);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());
        task = taskService.removeOneById(unknownUserId, id);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());
        task = taskService.removeOneById(userId, unknownId);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());

        task = taskService.removeOneById(userId, id);
        Assert.assertNotNull(task);
        Assert.assertEquals(counter - 1, taskService.count());
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
        task = taskService.findById(task1.getId());
        Assert.assertNull(task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIdWithoutUserId() throws AbstractException {
        loadData();
        taskService.removeOneById(null, task1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdWithoutId() throws AbstractException {
        loadData();
        taskService.removeOneById(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIdWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.removeOneById("", task1.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdWithEmptyId() throws AbstractException {
        loadData();
        taskService.removeOneById(task1.getUserId(), "");
    }

    @Test
    public void testRemoveByIndex() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String userId = task1.getUserId();
        final int index = 0;
        final int counter = taskService.count();

        @Nullable Task task = taskService.removeOneByIndex(unknownUserId, index);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());

        task = taskService.removeOneByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(counter - 1, taskService.count());
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
        task = taskService.findById(task1.getId());
        Assert.assertNull(task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIndexWithoutUserId() throws AbstractException {
        loadData();
        taskService.removeOneByIndex(null, 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexWithIndexLessRange() throws AbstractException {
        taskService.removeOneByIndex(task1.getUserId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexWithIndexMoreRange() throws AbstractException {
        loadData();
        taskService.removeOneByIndex(task1.getUserId(), 10);
    }

    @Test
    public void testRemoveByName() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownName = unknownTask.getName();
        @NotNull final String userId = task1.getUserId();
        @NotNull final String name = task1.getName();
        final int counter = taskService.count();

        @Nullable Task task = taskService.removeOneByName(unknownUserId, unknownName);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());
        task = taskService.removeOneByName(unknownUserId, name);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());
        task = taskService.removeOneByName(userId, unknownName);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskService.count());

        task = taskService.removeOneByName(userId, name);
        Assert.assertNotNull(task);
        Assert.assertEquals(counter - 1, taskService.count());
        Assert.assertEquals(task1.hashCode(), task.hashCode());
        Assert.assertEquals(task1, task);
        task = taskService.findById(task1.getId());
        Assert.assertNull(task);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByNameWithoutUserId() throws AbstractException {
        loadData();
        taskService.removeOneByName(null, task1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveByNameWithoutName() throws AbstractException {
        loadData();
        taskService.removeOneByName(task1.getUserId(), null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testRemoveByNameWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.removeOneByName("", task1.getName());
    }

    @Test(expected = EmptyNameException.class)
    public void testRemoveByNameWithEmptyName() throws AbstractException {
        loadData();
        taskService.removeOneByName(task1.getUserId(), "");
    }

    @Test
    public void testUpdateById() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownId = unknownTask.getId();
        @NotNull final String userId = task1.getUserId();
        @NotNull final String id = task1.getId();
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";

        @Nullable Task task = taskService.updateTaskById(
                unknownUserId,
                unknownId,
                newName,
                newDescription
        );
        Assert.assertNull(task);
        task = taskService.updateTaskById(
                unknownUserId,
                id,
                newName,
                newDescription
        );
        Assert.assertNull(task);
        task = taskService.updateTaskById(
                userId,
                unknownId,
                newName,
                newDescription
        );
        Assert.assertNull(task);

        task = taskService.updateTaskById(
                userId,
                id,
                newName,
                newDescription
        );
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
        task = taskService.findById(task.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithoutUserId() throws AbstractException {
        loadData();
        taskService.updateTaskById(
                null,
                task1.getId(),
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithoutId() throws AbstractException {
        loadData();
        taskService.updateTaskById(
                task1.getUserId(),
                null,
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithoutName() throws AbstractException {
        loadData();
        taskService.updateTaskById(
                task1.getUserId(),
                task1.getId(),
                null,
                task1.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIdWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.updateTaskById(
                "",
                task1.getId(),
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = EmptyIdException.class)
    public void testUpdateByIdWithEmptyId() throws AbstractException {
        loadData();
        loadData();
        taskService.updateTaskById(
                task1.getUserId(),
                "",
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIdWithEmptyName() throws AbstractException {
        loadData();
        taskService.updateTaskById(
                task1.getUserId(),
                task1.getId(),
                "",
                task1.getDescription()
        );
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String userId = task1.getUserId();
        final int index = 0;
        @NotNull final String newName = "New name1";
        @NotNull final String newDescription = "New description1";

        @Nullable Task task = taskService.updateTaskByIndex(
                unknownUserId,
                index,
                newName,
                newDescription
        );
        Assert.assertNull(task);

        task = taskService.updateTaskByIndex(
                userId,
                index,
                newName,
                newDescription
        );
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
        task = taskService.findOneByIndex(task.getUserId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithoutUserId() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                null,
                0,
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithoutName() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                task1.getUserId(),
                0,
                null,
                task1.getDescription()
        );
    }

    @Test(expected = EmptyUserIdException.class)
    public void testUpdateByIndexWithEmptyUserId() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                "",
                0,
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = EmptyNameException.class)
    public void testUpdateByIndexWithEmptyName() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                task1.getUserId(),
                0,
                "",
                task1.getDescription()
        );
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithIndexLessRange() throws AbstractException {
        taskService.updateTaskByIndex(
                task1.getUserId(),
                -1,
                task1.getName(),
                task1.getDescription()
        );
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithIndexMoreRange() throws AbstractException {
        loadData();
        taskService.updateTaskByIndex(
                task1.getUserId(),
                10,
                task1.getName(),
                task1.getDescription()
        );
    }

}
