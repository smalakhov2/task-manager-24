package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AbstractServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IService<Project> service = new ProjectService(repository);

    @NotNull
    private final Project project1 = new Project("Project1", null, "1");

    @NotNull
    private final Project project2 = new Project("Project2", null, "1");

    @NotNull
    private final Project project3 = new Project("Project3", null, "3");

    @NotNull
    final Project unknownProject = new Project("Unknown", null, "");

    @NotNull
    final List<Project> allProjects = new ArrayList<>(Arrays.asList(project1, project2, project3));

    @After
    public void after() {
        service.clear();
    }

    public void loadData() {
        service.add(allProjects);
    }

    @Test
    public void testCreateEmptyService() {
        Assert.assertEquals(0, service.count());
    }

    @Test
    public void testFindAll() {
        loadData();
        @NotNull final List<Project> projects = service.findAll();
        Assert.assertEquals(allProjects.size(), projects.size());
        Assert.assertEquals(allProjects.hashCode(), projects.hashCode());
        Assert.assertEquals(allProjects, projects);
    }

    @Test
    public void testClear() {
        loadData();
        service.clear();
        Assert.assertEquals(0, service.count());
    }

    @Test
    public void testContains() {
        loadData();

        @Nullable final Project project = null;
        Assert.assertFalse(service.contains(project));
        Assert.assertFalse(service.contains(unknownProject));
        Assert.assertTrue(service.contains(project1));
    }

    @Test
    public void testContainsWithId() {
        loadData();

        @Nullable final Project project = null;
        Assert.assertFalse(service.contains(project));
        Assert.assertFalse(service.contains(unknownProject.getId()));
        Assert.assertTrue(service.contains(project1.getId()));
    }

    @Test
    public void testCount() {
        Assert.assertEquals(0, service.count());
        service.add(allProjects);
        Assert.assertEquals(3, service.count());
    }

    @Test
    public void testAddOne() {
        final int counter = service.count();

        @Nullable final Project project = null;
        service.add(project);
        Assert.assertEquals(counter, service.count());

        service.add(project1);
        Assert.assertEquals(counter + 1, service.count());
        Assert.assertTrue(service.contains(project1));
    }

    @Test
    public void testAddCollection() {
        int counter = service.count();

        @Nullable List<Project> projects = null;
        service.add(projects);
        Assert.assertEquals(counter, service.count());

        projects = new ArrayList<>(Arrays.asList(null, project1, null));
        service.add(projects);
        Assert.assertEquals(counter + 1, service.count());
        Assert.assertTrue(service.contains(project1));
        service.clear();

        service.add(allProjects);
        counter += allProjects.size();
        Assert.assertEquals(counter, service.count());
        for (@NotNull final Project project : allProjects) {
            Assert.assertTrue(service.contains(project));
        }
    }

    @Test
    public void testRemoveById() throws EmptyIdException {
        loadData();
        final int counter = service.count();

        @Nullable Project project = service.removeById(unknownProject.getId());
        Assert.assertNull(project);
        Assert.assertEquals(counter, service.count());

        project = service.removeById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
        Assert.assertEquals(counter - 1, service.count());
        Assert.assertFalse(service.contains(project1));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveByIdWithoutId() throws EmptyIdException {
        service.removeById(null);
    }


    @Test
    public void testFindById() throws EmptyIdException {
        loadData();

        @Nullable Project project = service.findById(unknownProject.getId());
        Assert.assertNull(project);

        project = service.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.hashCode(), project.hashCode());
        Assert.assertEquals(project1, project);
    }

    @Test(expected = EmptyIdException.class)
    public void testFindByIdWithoutId() throws EmptyIdException {
        service.findById(null);
    }

    @Test
    public void testLoadCollection() {
        @Nullable List<Project> projects = null;
        service.load(projects);
        Assert.assertEquals(0, service.count());

        projects = new ArrayList<>(Arrays.asList(null, project1, null));
        service.load(projects);
        Assert.assertEquals(1, service.count());
        Assert.assertTrue(service.contains(project1));
        service.clear();

        service.add(unknownProject);
        service.load(allProjects);
        Assert.assertFalse(service.contains(unknownProject));
        Assert.assertEquals(allProjects.size(), service.count());
        for (@NotNull final Project project : allProjects) {
            Assert.assertTrue(service.contains(project));
        }
    }

}