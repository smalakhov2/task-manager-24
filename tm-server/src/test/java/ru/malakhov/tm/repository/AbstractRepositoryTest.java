package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.IRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AbstractRepositoryTest {

    @NotNull
    private final IRepository<Project> repository = new ProjectRepository();

    @NotNull
    private final Project projectOne = new Project("Project1", null, "1");

    @NotNull
    private final Project projectTwo = new Project("Project2", null, "1");

    @NotNull
    private final Project projectThree = new Project("Project3", null, "1");

    @NotNull
    final Project unknownProject = new Project("Unknown", null, "2");

    @NotNull
    final List<Project> allProjects = new ArrayList<>(Arrays.asList(projectOne, projectTwo, projectThree));

    @After
    public void after() {
        repository.clear();
    }

    public void loadData() {
        repository.add(allProjects);
    }

    @Test
    public void testCreateEmptyRepository() {
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void testFindAll() {
        loadData();
        @NotNull final List<Project> projects = repository.findAll();
        Assert.assertEquals(allProjects.hashCode(), projects.hashCode());
        Assert.assertEquals(allProjects.size(), projects.size());
        Assert.assertEquals(allProjects, projects);
    }

    @Test
    public void testClear() {
        loadData();
        repository.clear();
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void testAddOne() {
        final int counter = repository.count();
        repository.add(projectOne);
        Assert.assertEquals(counter + 1, repository.count());
        Assert.assertTrue(repository.contains(projectOne));
    }

    @Test
    public void testAddCollection() {
        int counter = repository.count();
        repository.add(allProjects);
        counter += allProjects.size();
        Assert.assertEquals(counter, repository.count());
        for (@NotNull final Project project : allProjects) {
            Assert.assertTrue(repository.contains(project));
        }
    }

    @Test
    public void testAddVarArg() {
        int counter = repository.count();
        repository.add(projectOne, projectTwo, projectThree);
        counter += 3;
        Assert.assertEquals(counter, repository.count());
        for (@NotNull final Project project : allProjects) {
            Assert.assertTrue(repository.contains(project));
        }
    }

    @Test
    public void testRemove() {
        loadData();
        final int counter = repository.count();

        @Nullable Project project = repository.remove(unknownProject);
        Assert.assertNull(project);
        Assert.assertEquals(counter, repository.count());

        project = repository.remove(projectOne);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
        Assert.assertFalse(repository.contains(projectOne));
    }

    @Test
    public void testRemoveById() {
        loadData();
        final int counter = repository.count();

        @Nullable Project project = repository.removeById(unknownProject.getId());
        Assert.assertNull(project);
        Assert.assertEquals(counter, repository.count());

        project = repository.removeById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
        Assert.assertFalse(repository.contains(projectOne));
    }

    @Test
    public void testFindById() {
        loadData();

        @Nullable Project project = repository.findById(unknownProject.getId());
        Assert.assertNull(project);

        project = repository.findById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
    }

    @Test
    public void testLoadCollection() {
        repository.add(unknownProject);
        repository.load(allProjects);

        Assert.assertFalse(repository.contains(unknownProject));
        Assert.assertEquals(allProjects.size(), repository.count());
        for (@NotNull final Project project : allProjects) {
            Assert.assertTrue(repository.contains(project));
        }
    }

    @Test
    public void testLoadVarArg() {
        repository.add(unknownProject);
        repository.load(projectOne, projectTwo, projectThree);

        Assert.assertFalse(repository.contains(unknownProject));
        Assert.assertEquals(allProjects.size(), repository.count());
        for (@NotNull final Project project : allProjects) {
            Assert.assertTrue(repository.contains(project));
        }
    }

    @Test
    public void testContains() {
        loadData();
        Assert.assertFalse(repository.contains(unknownProject));
        Assert.assertTrue(repository.contains(projectOne));
    }

    @Test
    public void testContainsWithId() {
        loadData();
        Assert.assertFalse(repository.contains(unknownProject.getId()));
        Assert.assertTrue(repository.contains(projectOne.getId()));
    }

    @Test
    public void testCount() {
        Assert.assertEquals(0, repository.count());
        repository.add(allProjects);
        Assert.assertEquals(3, repository.count());
    }

}
