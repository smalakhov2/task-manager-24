package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final Project projectOne = new Project("Project1", null, "1");

    @NotNull
    private final Project projectTwo = new Project("Project2", null, "2");

    @NotNull
    private final Project projectThree = new Project("Project3", null, "1");

    @NotNull
    private final Project projectFour = new Project("Project4", null, "2");

    @NotNull
    final Project unknownProject = new Project("Unknown", null, "3");

    @NotNull
    private final List<Project> userOneProjects = new ArrayList<>(Arrays.asList(projectOne, projectThree));

    @NotNull
    private final List<Project> userTwoProjects = new ArrayList<>(Arrays.asList(projectTwo, projectFour));

    private void loadData() {
        projectRepository.add(userOneProjects);
        projectRepository.add(userTwoProjects);
    }

    @After
    public void after() {
        projectRepository.clear();
    }

    @Test
    public void testCreateEmptyRepository() {
        Assert.assertEquals(0, projectRepository.count());
    }

    @Test
    public void testAddWithUserId() {
        final int counter = projectRepository.count();

        projectRepository.add(projectOne.getUserId(), projectOne);
        Assert.assertEquals(counter + 1, projectRepository.count());
        @Nullable final Project project = projectRepository.findById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
    }

    @Test
    public void testRemoveWithUserId() {
        loadData();
        int counter = projectRepository.count();

        projectRepository.remove(unknownProject.getUserId(), unknownProject);
        Assert.assertEquals(counter, projectRepository.count());

        projectRepository.remove(projectOne.getUserId(), projectOne);
        Assert.assertEquals(counter - 1, projectRepository.count());
        @Nullable final Project project = projectRepository.findById(projectOne.getId());
        Assert.assertNull(project);
    }

    @Test
    public void testFindAllWithUserId() {
        loadData();

        @NotNull List<Project> projects = projectRepository.findAll(unknownProject.getUserId());
        Assert.assertEquals(0, projects.size());

        projects = projectRepository.findAll(projectOne.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(userOneProjects.hashCode(), projects.hashCode());
        Assert.assertEquals(userOneProjects.size(), projects.size());
        Assert.assertEquals(userOneProjects, projects);
    }

    @Test
    public void testClearWithUserId() {
        loadData();
        int counter = projectRepository.count();

        projectRepository.clear(unknownProject.getUserId());
        Assert.assertEquals(counter, projectRepository.count());

        projectRepository.clear(projectOne.getUserId());
        counter -= userOneProjects.size();
        Assert.assertEquals(counter, projectRepository.count());
        for (@NotNull final Project project : userOneProjects) {
            Assert.assertFalse(projectRepository.contains(project));
        }
    }

    @Test
    public void testFindOneByIdWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownId = unknownProject.getId();
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String id = projectOne.getId();

        @Nullable Project project = projectRepository.findOneById(unknownUserId, unknownId);
        Assert.assertNull(project);
        project = projectRepository.findOneById(unknownUserId, id);
        Assert.assertNull(project);
        project = projectRepository.findOneById(userId, unknownId);
        Assert.assertNull(project);

        project = projectRepository.findOneById(userId, id);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
    }

    @Test
    public void testFindOneByIndexWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String userId = projectOne.getUserId();
        final int index = 0;

        @Nullable Project project = projectRepository.findOneByIndex(unknownUserId, index);
        Assert.assertNull(project);

        project = projectRepository.findOneByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
    }

    @Test
    public void testFindOneByNameWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownName = unknownProject.getName();
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String name = projectOne.getName();

        @Nullable Project project = projectRepository.findOneByName(unknownUserId, unknownName);
        Assert.assertNull(project);
        project = projectRepository.findOneByName(unknownUserId, name);
        Assert.assertNull(project);
        project = projectRepository.findOneByName(userId, unknownName);
        Assert.assertNull(project);

        project = projectRepository.findOneByName(userId, name);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
    }

    @Test
    public void testRemoveByIdWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownId = unknownProject.getId();
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String id = projectOne.getId();
        final int counter = projectRepository.count();

        @Nullable Project project = projectRepository.removeById(unknownUserId, unknownId);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());
        project = projectRepository.removeById(unknownUserId, id);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());
        project = projectRepository.removeById(userId, unknownId);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());

        project = projectRepository.removeById(userId, id);
        Assert.assertNotNull(project);
        Assert.assertEquals(counter - 1, projectRepository.count());
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
        project = projectRepository.findById(projectOne.getId());
        Assert.assertNull(project);
    }

    @Test
    public void testRemoveByIndexWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String userId = projectOne.getUserId();
        final int index = 0;
        final int counter = projectRepository.count();

        @Nullable Project project = projectRepository.removeByIndex(unknownUserId, index);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());

        project = projectRepository.removeByIndex(userId, index);
        Assert.assertNotNull(project);
        Assert.assertEquals(counter - 1, projectRepository.count());
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
        project = projectRepository.findById(projectOne.getId());
        Assert.assertNull(project);
    }

    @Test
    public void testRemoveByNameWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownProject.getUserId();
        @NotNull final String unknownName = unknownProject.getName();
        @NotNull final String userId = projectOne.getUserId();
        @NotNull final String name = projectOne.getName();
        final int counter = projectRepository.count();

        @Nullable Project project = projectRepository.removeByName(unknownUserId, unknownName);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());
        project = projectRepository.removeByName(unknownUserId, name);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());
        project = projectRepository.removeByName(userId, unknownName);
        Assert.assertNull(project);
        Assert.assertEquals(counter, projectRepository.count());

        project = projectRepository.removeByName(userId, name);
        Assert.assertNotNull(project);
        Assert.assertEquals(counter - 1, projectRepository.count());
        Assert.assertEquals(projectOne.hashCode(), project.hashCode());
        Assert.assertEquals(projectOne, project);
        project = projectRepository.findById(projectOne.getId());
        Assert.assertNull(project);
    }

}
