package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final Task taskOne = new Task("Task1", null, "1");

    @NotNull
    private final Task taskTwo = new Task("Task2", null, "2");

    @NotNull
    private final Task taskThree = new Task("Task3", null, "1");

    @NotNull
    private final Task taskFour = new Task("Task4", null, "2");

    @NotNull
    final Task unknownTask = new Task("Unknown", null, "3");

    @NotNull
    private final List<Task> userOneTasks = new ArrayList<>(Arrays.asList(taskOne, taskThree));

    @NotNull
    private final List<Task> userTwoTasks = new ArrayList<>(Arrays.asList(taskTwo, taskFour));

    private void loadData() {
        taskRepository.add(userOneTasks);
        taskRepository.add(userTwoTasks);
    }

    @After
    public void after() {
        taskRepository.clear();
    }

    @Test
    public void testCreateEmptyRepository() {
        Assert.assertEquals(0, taskRepository.count());
    }

    @Test
    public void testAddWithUserId() {
        final int counter = taskRepository.count();

        taskRepository.add(taskOne.getUserId(), taskOne);
        Assert.assertEquals(counter + 1, taskRepository.count());
        @Nullable final Task task = taskRepository.findById(taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);
    }

    @Test
    public void testRemoveWithUserId() {
        loadData();
        int counter = taskRepository.count();

        taskRepository.remove(unknownTask.getUserId(), unknownTask);
        Assert.assertEquals(counter, taskRepository.count());

        taskRepository.remove(taskOne.getUserId(), taskOne);
        Assert.assertEquals(counter - 1, taskRepository.count());
        @Nullable final Task task = taskRepository.findById(taskOne.getId());
        Assert.assertNull(task);
    }

    @Test
    public void testFindAllWithUserId() {
        loadData();

        @NotNull List<Task> tasks = taskRepository.findAll(unknownTask.getUserId());
        Assert.assertEquals(0, tasks.size());

        tasks = taskRepository.findAll(taskOne.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(userOneTasks.hashCode(), tasks.hashCode());
        Assert.assertEquals(userOneTasks.size(), tasks.size());
        Assert.assertEquals(userOneTasks, tasks);
    }

    @Test
    public void testClearWithUserId() {
        loadData();
        int counter = taskRepository.count();

        taskRepository.clear(unknownTask.getUserId());
        Assert.assertEquals(counter, taskRepository.count());

        taskRepository.clear(taskOne.getUserId());
        counter -= userOneTasks.size();
        Assert.assertEquals(counter, taskRepository.count());
        for (@NotNull final Task task : userOneTasks) {
            Assert.assertFalse(taskRepository.contains(task.getId()));
        }
    }

    @Test
    public void testFindOneByIdWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownId = unknownTask.getId();
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String id = taskOne.getId();

        @Nullable Task task = taskRepository.findOneById(unknownUserId, unknownId);
        Assert.assertNull(task);
        task = taskRepository.findOneById(unknownUserId, id);
        Assert.assertNull(task);
        task = taskRepository.findOneById(userId, unknownId);
        Assert.assertNull(task);

        task = taskRepository.findOneById(userId, id);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);
    }

    @Test
    public void testFindOneByIndexWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String userId = taskOne.getUserId();
        final int index = 0;

        @Nullable Task task = taskRepository.findOneByIndex(unknownUserId, index);
        Assert.assertNull(task);

        task = taskRepository.findOneByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);
    }

    @Test
    public void testFindOneByNameWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownName = unknownTask.getName();
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String name = taskOne.getName();

        @Nullable Task task = taskRepository.findOneByName(unknownUserId, unknownName);
        Assert.assertNull(task);
        task = taskRepository.findOneByName(unknownUserId, name);
        Assert.assertNull(task);
        task = taskRepository.findOneByName(userId, unknownName);
        Assert.assertNull(task);

        task = taskRepository.findOneByName(userId, name);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);
    }

    @Test
    public void testRemoveByIdWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownId = unknownTask.getId();
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String id = taskOne.getId();
        final int counter = taskRepository.count();

        @Nullable Task task = taskRepository.removeById(unknownUserId, unknownId);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());
        task = taskRepository.removeById(unknownUserId, id);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());
        task = taskRepository.removeById(userId, unknownId);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());

        task = taskRepository.removeById(userId, id);
        Assert.assertNotNull(task);
        Assert.assertEquals(counter - 1, taskRepository.count());
        Assert.assertEquals(taskOne, task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        task = taskRepository.findById(taskOne.getId());
        Assert.assertNull(task);
    }

    @Test
    public void testRemoveByIndexWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String userId = taskOne.getUserId();
        final int index = 0;
        final int counter = taskRepository.count();

        @Nullable Task task = taskRepository.removeByIndex(unknownUserId, index);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());

        task = taskRepository.removeByIndex(userId, index);
        Assert.assertNotNull(task);
        Assert.assertEquals(counter - 1, taskRepository.count());
        Assert.assertEquals(taskOne, task);
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        task = taskRepository.findById(taskOne.getId());
        Assert.assertNull(task);
    }

    @Test
    public void testRemoveByNameWithUserId() {
        loadData();
        @NotNull final String unknownUserId = unknownTask.getUserId();
        @NotNull final String unknownName = unknownTask.getName();
        @NotNull final String userId = taskOne.getUserId();
        @NotNull final String name = taskOne.getName();
        final int counter = taskRepository.count();

        @Nullable Task task = taskRepository.removeByName(unknownUserId, unknownName);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());
        task = taskRepository.removeByName(unknownUserId, name);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());
        task = taskRepository.removeByName(userId, unknownName);
        Assert.assertNull(task);
        Assert.assertEquals(counter, taskRepository.count());

        task = taskRepository.removeByName(userId, name);
        Assert.assertNotNull(task);
        Assert.assertEquals(counter - 1, taskRepository.count());
        Assert.assertEquals(taskOne.hashCode(), task.hashCode());
        Assert.assertEquals(taskOne, task);
        task = taskRepository.findById(taskOne.getId());
        Assert.assertNull(task);
    }

}
