package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.IntegrationCategory;

import java.util.List;

@Category(IntegrationCategory.class)
public final class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final String USER_LOGIN = "test";

    @NotNull
    private final String USER_PASSWORD = "test";

    @NotNull
    private final String ADMIN_LOGIN = "admin";

    @NotNull
    private final String ADMIN_PASSWORD = "admin";

    @After
    public void after() throws AbstractException_Exception {
        @Nullable final Session session = sessionEndpoint.openSession(ADMIN_LOGIN, ADMIN_PASSWORD);
        sessionEndpoint.clearAllSession(session);
    }

    @Test
    public void testOpenSession() throws AbstractException_Exception {
        @Nullable final Session session = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.id);
        Assert.assertNotNull(session.signature);
        Assert.assertNotNull(session.timestamp);
        Assert.assertNotNull(session.userId);
        @Nullable final Session incorrectSession = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD + "1");
        Assert.assertNull(incorrectSession);
    }

    @Test
    public void testCloseSession() throws Exception {
        @Nullable final Session session1 = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @Nullable final Session session2 = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @NotNull final Result result = sessionEndpoint.closeSession(session2);
        Assert.assertTrue(result.success);
        @NotNull final List<Session> session = sessionEndpoint.getListSession(session1);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void testCloseSessionAll() throws Exception {
        @Nullable final Session session1 = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @NotNull final Result result = sessionEndpoint.closeSessionAll(session1);
        Assert.assertTrue(result.success);
        @Nullable final Session session2 = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @NotNull final List<Session> session = sessionEndpoint.getListSession(session2);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void testGetListSession() throws Exception {
        @Nullable final Session userSession = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @Nullable final Session adminSession = sessionEndpoint.openSession(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final List<Session> userSessions = sessionEndpoint.getListSession(userSession);
        Assert.assertEquals(2, userSessions.size());
        @NotNull final List<Session> adminSessions = sessionEndpoint.getListSession(adminSession);
        Assert.assertEquals(1, adminSessions.size());
    }

    @Test
    public void testGetUser() throws AbstractException_Exception {
        @Nullable final Session userSession = sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @Nullable final User user = sessionEndpoint.getUser(userSession);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_LOGIN, user.getLogin());
        @Nullable final Session adminSession = sessionEndpoint.openSession(ADMIN_LOGIN, ADMIN_PASSWORD);
        @Nullable final User admin = sessionEndpoint.getUser(adminSession);
        Assert.assertNotNull(admin);
        Assert.assertEquals(ADMIN_LOGIN, admin.getLogin());
    }

    @Test
    public void testClearAllSessions() throws AbstractException_Exception {
        sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @Nullable Session adminSession = sessionEndpoint.openSession(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final Result result = sessionEndpoint.clearAllSession(adminSession);
        Assert.assertTrue(result.success);
        adminSession = sessionEndpoint.openSession(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final List<Session> sessions = sessionEndpoint.getAllSessionList(adminSession);
        Assert.assertEquals(1, sessions.size());
    }

    @Test
    public void testGetAllSessionsList() throws AbstractException_Exception {
        sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        sessionEndpoint.openSession(USER_LOGIN, USER_PASSWORD);
        @Nullable Session adminSession = sessionEndpoint.openSession(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final List<Session> sessions = sessionEndpoint.getAllSessionList(adminSession);
        Assert.assertEquals(3, sessions.size());
    }

}
