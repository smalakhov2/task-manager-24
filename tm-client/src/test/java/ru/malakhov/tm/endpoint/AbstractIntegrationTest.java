package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AbstractIntegrationTest {

    @NotNull
    protected final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    protected final Session adminSession = openAdminSession();

    @Nullable
    protected final Session userSession = openUserSession();

    @Nullable
    private Session openAdminSession() {
        try {
            return sessionEndpoint.openSession("admin", "admin");
        } catch (AbstractException_Exception e) {
            return null;
        }
    }

    @Nullable
    private Session openUserSession() {
        try {
            return sessionEndpoint.openSession("test", "test");
        } catch (AbstractException_Exception e) {
            return null;
        }
    }

}