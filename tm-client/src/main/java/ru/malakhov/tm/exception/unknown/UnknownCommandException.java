package ru.malakhov.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@NotNull final String command) {
        super("Error! ``" + command + "`` is unknown command...");
    }

}
