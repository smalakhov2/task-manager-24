package ru.malakhov.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@NotNull final String argument) {
        super("Error! ``" + argument + "`` is unknown argument...");
    }

}
