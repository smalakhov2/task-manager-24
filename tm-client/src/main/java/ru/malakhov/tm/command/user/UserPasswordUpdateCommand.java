package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "update-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update password.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[UPDATE-PASSWORD]");
        System.out.print("ENTER CURRENT PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PASSWORD: ");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getUserEndpoint().updateUserPassword(session, password, newPassword);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
