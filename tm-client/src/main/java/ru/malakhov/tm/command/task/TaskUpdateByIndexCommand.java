package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.endpoint.Task;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NEW NAME: ");
        @NotNull final String newName = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        @NotNull final String newDescription = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        @Nullable final Task taskUpsated = serviceLocator.getTaskEndpoint().updateTaskByIndex(
                session,
                index,
                newName,
                newDescription
        );
        if (taskUpsated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
