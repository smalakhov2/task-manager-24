package ru.malakhov.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.util.TerminalUtil;

public final class RegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registry.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().registryUser(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
