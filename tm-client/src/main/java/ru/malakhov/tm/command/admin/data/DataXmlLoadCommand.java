package ru.malakhov.tm.command.admin.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;

public final class DataXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load xml data.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[DATA XML LOAD]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getAdminDataEndpoint().loadDataXml(session);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
