package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Project;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        @NotNull final Project project = serviceLocator.getProjectEndpoint().getProjectById(session, id);
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
