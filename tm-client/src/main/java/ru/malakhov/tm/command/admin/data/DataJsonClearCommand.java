package ru.malakhov.tm.command.admin.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;

public final class DataJsonClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove JSON data.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[DATA JSON CLEAR]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getAdminDataEndpoint().clearDataJson(session);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
