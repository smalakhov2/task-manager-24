package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AccessDeniedException_Exception;
import ru.malakhov.tm.endpoint.Session;

public final class ProjectListClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws AccessDeniedException_Exception {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getProjectEndpoint().clearProjectList(session);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
