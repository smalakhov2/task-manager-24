package ru.malakhov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-lock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user account by login.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[LOCK-USER]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getAdminUserEndpoint().lockUserByLogin(session, login);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
