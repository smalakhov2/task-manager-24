package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AccessDeniedException_Exception;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws AccessDeniedException_Exception {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER PROJECT NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER PROJECT DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getProjectEndpoint().createProject(session, name, description);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
