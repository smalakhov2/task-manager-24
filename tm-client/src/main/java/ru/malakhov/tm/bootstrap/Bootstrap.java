package ru.malakhov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.exception.unknown.UnknownArgumentException;
import ru.malakhov.tm.exception.unknown.UnknownCommandException;
import ru.malakhov.tm.repository.CommandRepository;
import ru.malakhov.tm.repository.PropertyRepository;
import ru.malakhov.tm.service.CommandService;
import ru.malakhov.tm.service.PropertyService;
import ru.malakhov.tm.util.TerminalUtil;
import ru.malakhov.tm.endpoint.*;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyRepository propertyRepository = new PropertyRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService(propertyRepository);

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = adminDataEndpointService.getAdminDataEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @SneakyThrows
    private void initCommands() throws AbstractException_Exception {
        @NotNull final Reflections reflections = new Reflections("ru.malakhov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            try {
                registry(clazz.newInstance());
            }catch(Exception e ){
                throw new AbstractException_Exception();
            }
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            try {
                parseArg(arg);
            } catch (@NotNull final Exception e) {
                logError(e);
            }
        }
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands().values();
        for (@NotNull final AbstractCommand command : commands) {
            if (arg.equals(command.arg())) {
                command.execute();
                return;
            }
        }
        throw new UnknownArgumentException(arg);
    }

    private void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @NotNull final AbstractCommand command = commandService.getCommands().get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        if (command.secure() && !propertyService.isAuth()) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        command.execute();
    }

    public void run(@NotNull final String[] args) throws AbstractException_Exception {
        initCommands();
        parseArgs(args);
        displayWelcome();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (@NotNull final Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        System.out.println();
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public AdminDataEndpoint getAdminDataEndpoint() {
        return adminDataEndpoint;
    }

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

}