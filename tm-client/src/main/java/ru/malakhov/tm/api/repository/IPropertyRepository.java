package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.Session;

public interface IPropertyRepository {

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

}
