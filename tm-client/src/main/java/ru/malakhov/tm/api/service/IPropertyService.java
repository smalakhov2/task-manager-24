package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.Session;

public interface IPropertyService {

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

    boolean isAuth();

}
